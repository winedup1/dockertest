FROM python:3-onbuild
MAINTAINER Don Leaman winedup@gmail.com

COPY . /usr/src/app

CMD ["python", "api.py"]
